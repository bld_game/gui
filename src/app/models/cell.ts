export class Cell {
    public row_id: number;
    public cell_id: number;
    public value: string;
    public selected: boolean;

    constructor() {
    }
}
