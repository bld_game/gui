/*
    '1', 'игра создана'
'2', 'ждем соперника'
'3', 'ваш ход'
'4', 'ход соперника'
'5', 'победа'
'6', 'поражение'
*/

import { Observable } from 'rxjs';
import { Gamestep } from './gamestep';

export const enum gameStatusCode {
    created = 1,
    one_player_in_game = 2,
    two_player_in_game = 3,
    two_player_do_step = 4,
    one_player_win = 5,
    two_player_win = 6,
    nobody_win = 7,
    loading_data = 8
}

export const enum gameStatusText {
    created = 'игра создана',
    one_player_in_game = 'ждем соперника',
    two_player_in_game = 'ваш ход',
    two_player_do_step = 'ход соперника',
    one_player_win = 'победа',
    two_player_win = 'поражение',
    nobody_win = 'ничья',
    loading_data = 'загрузка данных...'
}

export const enum gameUrls {
    //root_api = '/bld_game/api_v1/game/', // for inet host
    root_api = 'http://localhost:8080/api_v1/game/',// for localhost
    root_angular = 'http://localhost:4200/', // for localhost
    list = 'list',
    connect = 'connect',
    info = 'info',
    check_step = 'checkstep',
    skip_step = 'skipstep'
}

export interface connectGame {
    connectGame(game_id: number): void;
}

export interface getInfoGame {
    getInfoGame(auth_key: string, game_id: number): Observable<Game>;
}

export interface setInfoGame {
    setInfoGame(gameInfo: Game): void;
}

export interface getCurrentGame {
    getCurrentGame(): Game;
}

export interface setCurrentGame {
    setCurrentGame(game_data: Game, game_id: number): boolean;
}

export interface checkStep {
    checkStep(auth_key: string, word: string, letter: string
        , col_id: number, row_id: number): void;
}

export interface skipStep {
    skipStep(auth_key: string): void;
}

export interface destroyCurrentGame {
    destroyCurrentGame(game: Game);
}

export class Game {
    id: number;
    status_id: gameStatusCode;
    auth_key: string;
    start_word: string;
    mySteps: Gamestep[] = [];
    opponentSteps: Gamestep[] = [];
    myScore: number = 0;
    opponentScore: number = 0;
    lastStepWord: string = "";
    current_seconds_for_step: number;
    default_seconds_for_step: number;

    constructor(id: number, status_id: number, auth_key?: string) {
        this.id = id;
        this.status_id = status_id;
        this.auth_key = auth_key;
        this.myScore = 0;
        this.current_seconds_for_step = 60;
        this.default_seconds_for_step = 60;
    }

    get status_name(): string {
        switch (+this.status_id) {
            case gameStatusCode.created:
                return gameStatusText.created;
            case gameStatusCode.one_player_in_game:
                return gameStatusText.one_player_in_game;
            case gameStatusCode.two_player_in_game:
                return gameStatusText.two_player_in_game;
            case gameStatusCode.two_player_do_step:
                return gameStatusText.two_player_do_step;
            case gameStatusCode.one_player_win:
                return gameStatusText.one_player_win;
            case gameStatusCode.two_player_win:
                return gameStatusText.two_player_win;
            case gameStatusCode.nobody_win:
                return gameStatusText.nobody_win;
            default:
                return gameStatusText.loading_data;
        }
    }
}
