import { Cell } from './cell';

export class Field {    
    fields: Cell[][];
    selected_fields: Cell[];    
    rows_count: number;
    cols_count: number;
    start_word_row: number;
    start_word: string;
}