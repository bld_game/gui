import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GamelistComponent } from './gamelist.component';

import { GamelistRoutingModule } from './gamelist-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [GamelistComponent],
  imports: [
    CommonModule,
    GamelistRoutingModule,
    RouterModule
  ]
})
export class GamelistModule { }
