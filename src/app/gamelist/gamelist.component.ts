import { Component, OnInit, OnDestroy } from '@angular/core';
import { GameService } from '../services/game/game.service';
import { Game } from '.././models/game';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-gamelist',
  templateUrl: './gamelist.component.html',
  styleUrls: ['./gamelist.component.css']
})
export class GamelistComponent implements OnInit, OnDestroy {

  game_list: Game[];
  reloadGameList = interval(10000);
  reloadGameListSubsription: Subscription;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.gameService.getGameList().subscribe((data: Game[]) => {
      this.game_list = new Array<Game>();
      for (const item_data of data) {
        const game_tmp = new Game(item_data.id, item_data.status_id);
        this.game_list.push(game_tmp);
      }
    });
    this.reloadGameListSubsription = this.reloadGameList.subscribe(() => {
      this.gameService.getGameList().subscribe((data: Game[]) => {
        this.game_list = new Array<Game>();
        for (const item_data of data) {
          const game_tmp = new Game(item_data.id, item_data.status_id);
          this.game_list.push(game_tmp);
        }
      });
    });
  }

  ngOnDestroy() {
    this.reloadGameListSubsription.unsubscribe();
  }

}
