import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  @Input() game_status: string;
  @Input() my_score: number;
  @Input() opponent_score: number;
  @Input() lastStepWord: string;
  @Input() seconds_for_step: number;

  constructor() { }

  ngOnInit() {
  }

}
