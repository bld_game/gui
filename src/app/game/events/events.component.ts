import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Game } from '../../models/game';
import { Field } from '../../models/field';
import { Cell } from '../../models/cell';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  @Output() resetChoosed = new EventEmitter();
  @Output() makeStepEmitter = new EventEmitter();
  @Output() gameSoundsToggleEmitter = new EventEmitter();
  @Input() current_game: Game;
  @Input() field: Field;
  @Input() cellWithSelectedLetter: Cell;
  @Input() gameSoundsEnabled: boolean;

  constructor() { }

  ngOnInit() {
  }

  reset() {
    this.resetChoosed.emit();
  }

  makeStep() {
    this.makeStepEmitter.emit();
  }

  gameSoundsToggle() {
    this.gameSoundsToggleEmitter.emit();
  }

}
