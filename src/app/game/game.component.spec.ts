import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EventsComponent } from './events/events.component';
import { FieldComponent } from './field/field.component';

import { GameComponent } from './game.component';
import { InfoComponent } from './info/info.component';
import { LettersComponent } from './letters/letters.component';
import { StepsComponent } from './steps/steps.component';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      declarations: [GameComponent,
        FieldComponent,
        LettersComponent,
        EventsComponent,
        StepsComponent, InfoComponent
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
