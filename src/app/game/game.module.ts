import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { FieldComponent } from './field/field.component';
import { LettersComponent } from './letters/letters.component';
import { EventsComponent } from './events/events.component';

import { GameRoutingModule } from './game-routing.module';
import { StepsComponent } from './steps/steps.component';
import { InfoComponent } from './info/info.component';

@NgModule({
  declarations: [GameComponent,
    FieldComponent,
    LettersComponent,
    EventsComponent,
    StepsComponent, InfoComponent
  ],
  imports: [
    CommonModule,
    GameRoutingModule
  ]
})
export class GameModule { }
