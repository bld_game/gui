import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Letter } from '../../models/letter';
import { Cell } from '../../models/cell';
import { gameStatusCode } from '../../models/game';

@Component({
  selector: 'app-letters',
  templateUrl: './letters.component.html',
  styleUrls: ['./letters.component.css']
})
export class LettersComponent implements OnInit {

  @Input() letters: Letter[] = [];
  @Input() start_word = '';
  @Input() selectedFields: Cell[] = [];
  @Input() game_status = 0;
  @Output() selectedLetter: EventEmitter<string> = new EventEmitter();
  selectedLetterIndex = -1;

  constructor() { }

  ngOnInit() {
  }

  selectLetter(letter: Letter, index: number) {
    if (!letter.selected && this.selectedFields.length === 0 && this.start_word.length > 0
      && this.game_status === gameStatusCode.two_player_in_game) {
      if (this.selectedLetterIndex > -1) {
        this.letters[this.selectedLetterIndex].selected = false;
      }
      letter.selected = !letter.selected;
      this.selectedLetterIndex = index;
      this.selectedLetter.emit(letter.value);
    }
  }

}
