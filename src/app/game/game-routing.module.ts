import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game.component';
import { AuthGuard } from '../services/auth/auth.guard';

const routes: Routes = [
  { path: '', component: GameComponent
  , canActivate: [AuthGuard]
  , canDeactivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
