import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Field } from "../../models/field";
import { Cell } from "../../models/cell";

@Component({
  selector: "app-field",
  templateUrl: "./field.component.html",
  styleUrls: ["./field.component.css"],
})
export class FieldComponent implements OnInit {
  @Input() field: Field;
  @Input() selectedLetter: string;
  @Input() selectedWord: string;
  @Input() cellWithSelectedLetter: Cell;
  @Output() selectWordEmitter = new EventEmitter();

  constructor() {}

  ngOnInit() {
    console.log("ngOnInit");
    this.initField();
    if (this.field && this.field.start_word && this.field.fields) {
      const start_word_array = this.field.start_word.split("");
      for (let _i = 0; _i < this.field.rows_count; _i++) {
        const row = [];
        for (let _j = 0; _j < this.field.cols_count; _j++) {
          const cell = { row_id: _i, cell_id: _j, value: "", selected: false };
          if (_i === this.field.start_word_row && start_word_array[_j]) {
            cell.value = start_word_array[_j];
          }
          row.push(cell);
        }
        this.field.fields.push(row);
      }
    }
    console.log(this.field);
  }

  private initField() {
    for (let row_id = 0; row_id < this.field.cols_count; row_id++) {
      this.field.fields[row_id] = [];
      for (let cell_id = 0; cell_id < this.field.cols_count; cell_id++) {
        const cell = new Cell();
        cell.row_id = row_id;
        cell.cell_id = cell_id;
        cell.selected = false;
        this.field.fields[row_id].push(cell);
      }
    }
  }

  setStartWord() {
    console.log("setStartWord");
    console.log(this.field);
    if (this.field && this.field.start_word && this.field.fields) {
      const start_word_array = this.field.start_word.split("");
      for (let _j = 0; _j < this.field.cols_count; _j++) {
        if (!this.field.fields[this.field.start_word_row]) {
          this.field.fields[this.field.start_word_row] = [];
        }
        this.field.fields[this.field.start_word_row][_j].value =
          start_word_array[_j];
      }
    }
  }

  onSelectCell(cell: Cell) {
    if (this.allowSetLetterOnField(cell)) {
      this.selectWordEmitter.emit(cell);
    }
  }

  allowSetLetterOnField(cell: Cell): boolean {
    /*
    проверяем можно ли ставит в поле выбранную букву:
    установка буквы разрешена только рядом с текущими буквами на поле.
    */
    let allow = false;
    if (this.field && this.field.fields) {
      const start_row = cell.row_id - 1 > 0 ? cell.row_id - 1 : 0;
      const end_row =
        cell.row_id + 1 < this.field.rows_count ? cell.row_id + 1 : cell.row_id;
      const start_cell = cell.cell_id - 1 > 0 ? cell.cell_id - 1 : 0;
      const end_cell =
        cell.cell_id + 1 < this.field.cols_count
          ? cell.cell_id + 1
          : cell.cell_id;
      for (let _i = start_row; _i <= end_row; _i++) {
        if (
          this.field.fields[_i][cell.cell_id].value !== "" &&
          (_i !== this.cellWithSelectedLetter.row_id ||
            cell.cell_id !== this.cellWithSelectedLetter.cell_id)
        ) {
          allow = true;
          break;
        }
      }
      if (!allow) {
        for (let _i = start_cell; _i <= end_cell; _i++) {
          if (
            this.field.fields[cell.row_id][_i].value !== "" &&
            (_i !== this.cellWithSelectedLetter.cell_id ||
              cell.row_id !== this.cellWithSelectedLetter.row_id)
          ) {
            allow = true;
            break;
          }
        }
      }
    }
    return allow;
  }
}
