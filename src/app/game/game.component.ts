import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { interval, Subscription, range } from 'rxjs';
import { filter } from 'rxjs/operators';
import { GameService } from '../services/game/game.service';
import { DialogService } from '../services/dialog/dialog.service';
import { Cell } from '../models/cell';
import { Field } from '../models/field';
import { Letter } from '../models/letter';
import { Game, gameStatusCode } from '../models/game';
import { FieldComponent } from './field/field.component';
import { Howl } from 'howler';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {

  field: Field = { rows_count: 5, cols_count: 5, start_word_row: 2, fields: [], selected_fields: [], start_word: '' };
  letters: Letter[] = [];
  selectedLetter = '';
  selectedWord = '';
  cellWithSelectedLetter: Cell = { row_id: -1, cell_id: -1, value: '', selected: false };
  current_game: Game = this.gameService.getCurrentGame();
  reloadGameInfo = interval(10000);
  reloadGameInfoSubscription: Subscription;
  stepTimer = interval(1000);
  stepTimerSubscription: Subscription;
  gameLoop$: Subscription;
  gameSoundsEnabled = true;
  sound = new Howl({
    src: ['./assets/yourstep.webm', './assets/yourstep.mp3', './assets/yourstep.mp4']
    , volume: 0.8,
  });
  onMakeStepCompleted = true;
  source$ = range(0, 10);
  @ViewChild(FieldComponent)
  private fieldComponent: FieldComponent;

  constructor(private gameService: GameService, public dialogService: DialogService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.installGame();
  }

  ngOnDestroy() {
    this.reloadGameInfoSubscription.unsubscribe();
    this.current_game = null;
    this.gameService.destroyCurrentGame();
  }

  installGame() {
    const game_id = this.route.parent.snapshot.paramMap.get('id');
    const first_letter_code = 1040, last_letter_code = 1072;
    for (let _i = first_letter_code; _i < last_letter_code; _i++) {
      const char = String.fromCharCode(_i);
      this.letters.push({ value: char, selected: false });
    }
    this.reloadGameInfo.subscribe(() => {
      console.log(`this.current_game`);
      console.log(this.current_game);
      this.gameService.getInfoGame(this.current_game.auth_key, game_id as unknown as number)
        .subscribe((response: Game) => {
          if (this.current_game.status_id === gameStatusCode.two_player_do_step
            && this.gameSoundsEnabled
            && response.status_id !== this.current_game.status_id) {
            this.current_game.current_seconds_for_step = this.current_game.default_seconds_for_step;
            this.sound.play();
          }
          this.gameService.setInfoGame(response);
          if ((!this.field.start_word || this.field.start_word.length === 0)
            && this.current_game.status_id > gameStatusCode.one_player_in_game) {
            this.field.start_word = this.current_game.start_word;
            this.fieldComponent.setStartWord();
          }
          if (this.current_game.mySteps && this.current_game.mySteps.length > 0) {
            for (const step of this.current_game.mySteps) {
              this.field.fields[step.row_id][step.col_id].value = step.letter;
            }
          }
          if (this.current_game.opponentSteps && this.current_game.opponentSteps.length > 0) {
            for (const step of this.current_game.opponentSteps) {
              this.field.fields[step.row_id][step.col_id].value = step.letter;
            }
          }
          if (this.current_game.status_id === gameStatusCode.one_player_win
            || this.current_game.status_id === gameStatusCode.two_player_win
            || this.current_game.status_id === gameStatusCode.nobody_win) {
            this.reloadGameInfoSubscription.unsubscribe();
          }
        });
    });
    this.stepTimerSubscription = this.stepTimer
      .pipe(filter(() => this.current_game.current_seconds_for_step > 0
        && this.field.start_word.length > 0))
      .subscribe(() => {
        console.log(this.current_game.current_seconds_for_step);
        if (--this.current_game.current_seconds_for_step === 0
          && this.current_game.status_id === gameStatusCode.two_player_in_game) {
          this.gameService.skipStep(this.current_game.auth_key).subscribe(
            (response: any) => {
              if (response.success) {
                this.current_game.current_seconds_for_step = this.current_game.default_seconds_for_step;
              }
            }
          );
        }
      });
  }

  onSelectLetter(selectedLetter: string): boolean {
    if (this.field.start_word.length === 0) {
      return false;
    }
    this.selectedLetter = selectedLetter;
    return true;
  }

  onResetChoosed() {
    console.log('onResetChoosed');
    console.log(this.cellWithSelectedLetter);
    this.selectedWord = '';
    for (let _i = 0; _i < this.field.rows_count; _i++) {
      for (let _j = 0; _j < this.field.cols_count; _j++) {
        this.field.fields[_i][_j].selected = false;
        if (_i === this.cellWithSelectedLetter.row_id
          && _j === this.cellWithSelectedLetter.cell_id) {
          this.field.fields[_i][_j].value = '';
        }
      }
    }
    this.field.selected_fields = [];
    this.cellWithSelectedLetter.row_id = -1;
    this.cellWithSelectedLetter.cell_id = -1;
    this.cellWithSelectedLetter.value = '';
    this.cellWithSelectedLetter.selected = false;
  }

  onSelectWord(selectedFieldCell: Cell) {
    if ((this.cellWithSelectedLetter.row_id !== selectedFieldCell.row_id
      || this.cellWithSelectedLetter.cell_id !== selectedFieldCell.cell_id)
      && selectedFieldCell.value === ''
      && !this.cellWithSelectedLetter.selected) {
      console.log('устанавливаем букву на поле');
      this.cellWithSelectedLetter.row_id = selectedFieldCell.row_id;
      this.cellWithSelectedLetter.cell_id = selectedFieldCell.cell_id;
      this.cellWithSelectedLetter.value = this.selectedLetter;
    } else if ((!selectedFieldCell.selected
      && selectedFieldCell.value !== '' && this.cellWithSelectedLetter.value !== '')
      || (this.cellWithSelectedLetter.row_id === selectedFieldCell.row_id
        && this.cellWithSelectedLetter.cell_id === selectedFieldCell.cell_id)) {
      if (this.field.selected_fields.length === 0 || (this.field.selected_fields.length > 0 &&
        ((Math.abs(selectedFieldCell.row_id - this.field.selected_fields[this.field.selected_fields.length - 1].row_id) === 1
          && selectedFieldCell.cell_id === this.field.selected_fields[this.field.selected_fields.length - 1].cell_id)
          ||
          (Math.abs(selectedFieldCell.cell_id - this.field.selected_fields[this.field.selected_fields.length - 1].cell_id) === 1
            && selectedFieldCell.row_id === this.field.selected_fields[this.field.selected_fields.length - 1].row_id)
        )
      )
      ) {
        console.log('выбираем слово');
        this.field.selected_fields.push(selectedFieldCell);
        this.field.fields[selectedFieldCell.row_id][selectedFieldCell.cell_id].selected = true;
        if (this.cellWithSelectedLetter.row_id === selectedFieldCell.row_id
          && this.cellWithSelectedLetter.cell_id === selectedFieldCell.cell_id) {
          this.selectedWord += this.cellWithSelectedLetter.value;
          this.cellWithSelectedLetter.selected = true;
        } else {
          this.selectedWord += selectedFieldCell.value;
        }
      }
    }
  }

  onMakeStep() {
    console.log('onMakeStep');
    console.log(this.cellWithSelectedLetter);
    if (this.onMakeStepCompleted) {
      this.onMakeStepCompleted = false;
      this.gameService.checkStep(this.current_game.auth_key, this.selectedWord
        , this.cellWithSelectedLetter.value
        , this.cellWithSelectedLetter.cell_id, this.cellWithSelectedLetter.row_id)
        .subscribe((response: any) => {
          console.log(response);
          if (response.success) {
            this.gameService.getInfoGame(this.current_game.auth_key, this.current_game.id)
              .subscribe((responseInfoGame: Game) => {
                this.onResetChoosed();
                this.gameService.setInfoGame(responseInfoGame);
                if (!this.field.start_word || this.field.start_word.length === 0
                  && this.current_game.status_id > gameStatusCode.one_player_in_game) {
                  this.field.start_word = this.current_game.start_word;
                  this.fieldComponent.setStartWord();
                }
                if (this.current_game.mySteps && this.current_game.mySteps.length > 0) {
                  for (const step of this.current_game.mySteps) {
                    this.field.fields[step.row_id][step.col_id].value = step.letter;
                  }
                }
                if (this.current_game.opponentSteps && this.current_game.opponentSteps.length > 0) {
                  for (const step of this.current_game.opponentSteps) {
                    this.field.fields[step.row_id][step.col_id].value = step.letter;
                  }
                }
                // reset letters field begin
                for (const letter of this.letters) {
                  letter.selected = false;
                }
                this.selectedLetter = '';
                // reset letters field end;
              });
          } else {
            this.onResetChoosed();
          }
          this.onMakeStepCompleted = true;
        });
    }
  }

  onGameSoundsToggle() {
    this.gameSoundsEnabled = !this.gameSoundsEnabled;
  }

}
