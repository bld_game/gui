import { Component, OnInit, Input } from '@angular/core';
import { Gamestep } from '../../models/gamestep';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css']
})
export class StepsComponent implements OnInit {

  @Input() title: string;
  @Input() steps: Gamestep[] = [];

  constructor() { }

  ngOnInit() {
  }

}
