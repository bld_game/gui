import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  Game, gameUrls, connectGame, getInfoGame
  , getCurrentGame, setCurrentGame, setInfoGame, checkStep, destroyCurrentGame
  , skipStep
} from '../../models/game';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GameService
  implements connectGame, getInfoGame, getCurrentGame, setCurrentGame
  , setInfoGame, checkStep, skipStep, destroyCurrentGame {

  private game_list_url = gameUrls.root_api + gameUrls.list;
  private game_connect_url = gameUrls.root_api + gameUrls.connect;
  private game_info_url = gameUrls.root_api + gameUrls.info;
  private game_check_step_url = gameUrls.root_api + gameUrls.check_step;
  private game_skip_step_url = gameUrls.root_api + gameUrls.skip_step;
  private current_game: Game;

  constructor(private http: HttpClient) { }

  getGameList(): Observable<Game[]> {
    return this.http.get<Game[]>(this.game_list_url);
  }

  connectGame(game_id: number): Observable<Game> {
    const body = { 'id': game_id.toString() };
    return this.http.post<Game>(this.game_connect_url, body, httpOptions);
  }

  getInfoGame(auth_key: string, game_id: number): Observable<Game> {
    const body = { 'auth_key': auth_key, 'id':  game_id};
    return this.http.post<Game>(this.game_info_url, body, httpOptions);
  }

  setCurrentGame(game_data: Game, game_id: number): boolean {
    localStorage.setItem('id', '' + game_id);
    localStorage.setItem('status', '' + game_data.status_id);
    localStorage.setItem('auth_key', game_data.auth_key);
    this.current_game = new Game(game_data.id, game_data.status_id, game_data.auth_key);
    return true;
  }

  getCurrentGame(): Game {
    return this.current_game;
  }

  getGameFromLocalStorage(): boolean {
    console.log('getGameFromLocalStorage');
    const id = localStorage.getItem('id');
    const status = localStorage.getItem('status');
    const auth_key = localStorage.getItem('auth_key');
    if (auth_key) {
      this.current_game = new Game(+id, +status, auth_key);
      return true;
    }
    return false;
  }

  destroyCurrentGame() {
    this.current_game = null;
    localStorage.clear();
  }

  setInfoGame(gameInfo: Game) {
    if (gameInfo) {
      /*console.log('setInfoGame');
      console.log(gameInfo);*/
      this.current_game.mySteps = gameInfo.mySteps;
      this.current_game.opponentSteps = gameInfo.opponentSteps;
      this.current_game.status_id = gameInfo.status_id;
      if (!this.current_game.start_word) {
        this.current_game.start_word = gameInfo.start_word;
      }
      this.current_game.myScore = +gameInfo.myScore;
      this.current_game.opponentScore = +gameInfo.opponentScore;
      this.current_game.lastStepWord = gameInfo.lastStepWord;
    }
  }

  checkStep(auth_key: string, word: string, letter: string, col_id: number, row_id: number): Observable<any> {
    const body = {
      'auth_key': auth_key, 'word': word
      , 'letter': letter, 'col_id': col_id, 'row_id': row_id
    };
    return this.http.post<any>(this.game_check_step_url, body, httpOptions);
  }

  skipStep(auth_key: string): Observable<any> {
    const body = {
      'auth_key': auth_key
    };
    return this.http.post<any>(this.game_skip_step_url, body, httpOptions);
  }

}
