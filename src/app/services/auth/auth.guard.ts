import { Injectable } from '@angular/core';
import { CanActivate, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Params } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GameService } from '../game/game.service';
import { Game, gameStatusCode } from '../../models/game';
import { GameComponent } from '../../game/game.component';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanDeactivate<GameComponent> {

  constructor(private gameService: GameService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | boolean {
    const params = route.params;
    console.log(params.id);
    return this.checkLogin(params);
  }

  canDeactivate(
    component: GameComponent
  ): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (component.current_game.status_id < gameStatusCode.two_player_in_game) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    return component.dialogService.confirm('Если вы выйдете из игры, то игра будет проиграна! Все равно выйти?');
  }

  checkLogin(params: Params): Observable<boolean> | boolean {
    if (this.gameService.getGameFromLocalStorage()) {
      return true;
    } else {
      return this.gameService.connectGame(params.id).pipe(
        map((response: Game) => {
          if (response.auth_key) {
            return this.gameService.setCurrentGame(response, params.id as number);
          }
          this.router.navigate(['/list']);
          return false;
        }),
        catchError(error => of(false))
      );
    }
  }
}
